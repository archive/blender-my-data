import pathlib
import unittest

import mypy.api

import mydata

test_modules = ['mydata', 'mydata_benchmarks', 'mydata_main']


class MypyRunnerTest(unittest.TestCase):
    def test_run_mypy(self):
        proj_root = pathlib.Path(mydata.__file__).parent.parent
        args = ['--incremental'] + [str(proj_root / dirname) for dirname in test_modules]
        result = mypy.api.run(args)

        stdout, stderr, status = result

        messages = []
        if stderr:
            messages.append(stderr)
        if stdout:
            messages.append(stdout)
        if status:
            messages.append('Mypy failed with status %d' % status)
        if messages:
            self.fail('\n'.join(['Mypy errors:'] + messages))
