from pathlib import Path

BASE_DIR = Path(__file__).absolute().parent.parent
SECRET_KEY = '-set-in-settings-py-'
DEBUG = False

ALLOWED_HOSTS = ['-set-in-settings-py-']
USE_X_FORWARDED_HOST = True

STATIC_URL = '/static/'
STATIC_ROOT = BASE_DIR / 'static'
STATICFILES_DIRS = [BASE_DIR / 'webstatic']

INSTALLED_APPS = [
    'django.contrib.admindocs',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'blender_id_oauth_client',
    'mydata_benchmarks.apps.MydataBenchmarksConfig',
    'mydata_main.apps.MydataMainConfig',
    'debug_toolbar',
]

MIDDLEWARE = [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'mydata.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'mydata_main.context_processors.settings',
            ],
        },
    },
]

WSGI_APPLICATION = 'mydata.wsgi.application'


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': str(BASE_DIR / 'mydata.sqlite3'),
        'TEST': {
            'NAME': str(BASE_DIR / 'mydata-test.sqlite3'),
        },
    },
}


AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTHENTICATION_BACKENDS = (
    # 'web.oauth.blender.BlenderIdOauth2',
    # 'web.oauth.blender.BlenderIdToken',
    'django.contrib.auth.backends.ModelBackend',
    # 'mydata_benchmarks.token_auth.'
)

CSRF_FAILURE_VIEW = 'mydata_main.views.csrf_failure'

# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/
TIME_ZONE = 'UTC'
USE_TZ = True

BLENDER_ID = {
    # MUST end in a slash:
    'BASE_URL': 'http://id.local:8000/',
    'OAUTH_CLIENT': '-set-in-config-py-',
    'OAUTH_SECRET': '-set-in-config-py-'
}

BLENDER_OPENDATA = {
    # MUST end in a slash:
    'BASE_URL': 'http://opendata.local:8002/',
    'TOKEN': '-set-in-config-py-',
}

LOGIN_URL = '/oauth/login'
LOGOUT_URL = '/oauth/logout'
LOGIN_REDIRECT_URL = '/'

LATEST_CLIENT_VERSIONS = {
    # Latest version of the Benchmark Client, set to the empty string to skip version checks.
    'BENCHMARK': '1.0b2',
}
