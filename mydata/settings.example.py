"""
Local development settings.

Copy to settings.py and edit to suit your needs.
"""

import os

# noinspection PyUnresolvedReferences
from mydata.common_settings import *

DEBUG = True

# Update this to something unique for your machine.
# This was generated using "pwgen -sync 64"
SECRET_KEY = r'''d\~w_5m\zTAb}^eSmsTY=%eg:%X[+QQm?"@h~><+xrwojkqi,dl_F^n#3JR4Y/s{'''

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'mydata',
        'USER': 'mydata',
        'PASSWORD': 'mydata',
        'HOST': 'localhost',
        'PORT': 5432,
        'CONN_MAX_AGE': 0,
        'ATOMIC_REQUESTS': True,
        'TEST': {
            'NAME': 'mydata-test'
        },
    }
    # 'default': {
    #     'ENGINE': 'django.db.backends.sqlite3',
    #     'NAME': str(BASE_DIR / 'db.sqlite3'),
    # },
}


LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'default': {
            'format': '%(asctime)-15s %(levelname)8s %(name)s %(message)s'
        },
        'verbose': {
            'format': '%(asctime)-15s %(levelname)8s %(name)s %(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'default',  # Set to 'verbose' in production
            'stream': 'ext://sys.stderr',
        },
        # Enable this in production:
        # 'sentry': {
        #     'level': 'ERROR',  # To capture more than ERROR, change to WARNING, INFO, etc.
        #     'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
        #     # 'tags': {'custom-tag': 'x'},
        # },
    },
    'loggers': {
        'mydata': {'level': 'DEBUG'},
        'mydata_benchmarks': {'level': 'DEBUG'},
        # Disable this in production:
        'sentry.errors': {'level': 'DEBUG', 'handlers': ['console'], 'propagate': False},
    },
    'root': {
        'level': 'WARNING',
        'handlers': [
            'console',
            # Enable this in production:
            # 'sentry',
        ],
    }
}

# For Debug Toolbar, extend with whatever address you use to connect
# to your dev server.
INTERNAL_IPS = ['127.0.0.1']

ALLOWED_HOSTS = ['mydata.local']

# # Raven is the Sentry.io integration app for Django. Enable this on production:
# import raven
# INSTALLED_APPS.append('raven.contrib.django.raven_compat')
#
# RAVEN_CONFIG = {
#     'dsn': 'https://<key>:<secret>@sentry.io/<project>',
#     # If you are using git, you can also automatically configure the
#     # release based on the git info.
#     'release': raven.fetch_git_sha(BASE_DIR),
# }

BLENDER_ID = {
    # MUST end in a slash:
    'BASE_URL': 'http://id.local:8000/',
    'OAUTH_CLIENT': '-set-in-config-py-',
    'OAUTH_SECRET': '-set-in-config-py-'
}

# Used to send data over to the Open Data portal
BLENDER_OPENDATA = {
    # MUST end in a slash:
    'BASE_URL': 'http://opendata.local:8002/',
    'TOKEN': '-set-in-settings-py-',  # Generate this via the Open Data admin
}

os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'  # In production this must be 0 or not set
