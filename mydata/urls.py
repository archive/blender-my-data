from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path

import mydata_main.views

urlpatterns = [
    url(r'^', include('mydata_benchmarks.urls')),
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('admin/', admin.site.urls),
    path('oauth/', include('blender_id_oauth_client.urls', namespace='oauth')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

handler400 = mydata_main.views.ErrorView.as_view(template_name='errors/400.html', status=400)
handler403 = mydata_main.views.ErrorView.as_view(template_name='errors/403.html', status=403)
handler404 = mydata_main.views.ErrorView.as_view(template_name='errors/404.html', status=404)
handler500 = mydata_main.views.ErrorView.as_view(template_name='errors/500.html', status=500)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
