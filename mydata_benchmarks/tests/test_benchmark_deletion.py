import json

from django.urls import reverse

from .abstract_benchmark_test import AbstractBenchmarkTest, httpmock


class BenchmarkDeletionTest(AbstractBenchmarkTest):

    @httpmock.activate
    def test_delete_single_benchmark(self):
        from mydata_benchmarks import models, opendata

        # Create 3 benchmarks and place the first of the list in db_benchmark
        initial_benchmarks = 3
        db_benchmark: models.Benchmark = self.submit_benchmarks_default(count=initial_benchmarks)[0]

        # Ensure the presence of the Benchmarks in the database
        self.assertEqual(initial_benchmarks, models.Benchmark.objects.count())

        # Create mock for happy deletion
        self.opendata_mock_happy_deletion(db_benchmark.manage_id)

        # Use bulk delete API to delete the benchmark.
        self.client.force_login(self.user)
        resp = self.client.post(reverse('delete_bulk'),
                                data={'samples[]': str(db_benchmark.pk)})

        self.assertEqual(200, resp.status_code)
        # Check the response content
        self.assertEquals('Sample deleted', resp.json()['message'])

        # Check that only the Benchmark was deleted
        with self.assertRaises(models.Benchmark.DoesNotExist):
            models.Benchmark.objects.get(pk=db_benchmark.id)
        # Check that other benchmarks still exist
        self.assertEqual(initial_benchmarks - 1, models.Benchmark.objects.count())

    @httpmock.activate
    def test_multiple_benchmarks_deletion_happy(self):
        """Create 4 benchmarks and successfully delete 2 of them."""
        initial_benchmarks_count = 4
        benchmarks_to_delete_count = 2
        # Create 4 benchmarks and place the last two of the list in benchmarks_to_delete
        benchmarks_to_delete = self.submit_benchmarks_default(
            count=initial_benchmarks_count)[-benchmarks_to_delete_count:]

        # Create mock responses for benchmark deletions
        for i in range(benchmarks_to_delete_count):
            self.opendata_mock_happy_deletion(benchmarks_to_delete[i].manage_id)

        self.client.force_login(self.user)

        # Build list of ids to be deleted
        ids_to_delete = [str(b.id) for b in benchmarks_to_delete]
        # Use bulk delete API to delete the benchmark.
        resp = self.client.post(reverse('delete_bulk'), data={'samples[]': ids_to_delete})
        self.assertEqual(200, resp.status_code)

        # Check the response content
        resp_json = resp.json()
        self.assertEquals(f'{benchmarks_to_delete_count} samples deleted', resp_json['message'])
        self.assertEqual(resp_json['results']['samplesDeleted'],
                         [b.id for b in benchmarks_to_delete])
        httpmock.reset()

    @httpmock.activate
    def test_delete_non_existing_benchmark_on_opendata(self):
        """Try deleting a benchmark that exists on MyData, but not on OpenData."""
        from mydata_benchmarks import models, opendata

        # Create 4 benchmarks and place the first of the list in db_benchmark
        initial_benchmarks = 4
        db_benchmark: models.Benchmark = self.submit_benchmarks_default(count=initial_benchmarks)[0]

        # Mock the OpenData response for deleting the benchmark
        httpmock.add(
            'DELETE', opendata.url(f'api/benchmarks/{db_benchmark.manage_id}'),
            json={'message': 'Not Found'},
            status=404,
        )

        # Use bulk delete API to delete the benchmark.
        self.client.force_login(self.user)
        resp = self.client.post(reverse('delete_bulk'), data={'samples[]': str(db_benchmark.pk)})

        self.assertEqual(200, resp.status_code)
        # If no successful deletion happened, no message is provided
        self.assertIsNone(resp.json()['message'])
        # Ensure presence of one not deleted sample in the results
        self.assertEqual(1, len(resp.json()['results']['samplesNotDeleted']))
        # Ensure that the key and message in the error matches the sample we tried to delete
        result = resp.json()['results']['samplesNotDeleted'][0]
        expected_message = f"Server error on {opendata.url('api/benchmarks/')} with code 404"
        response_message = result['message']
        expected_key = db_benchmark.id
        response_key = result['key']
        self.assertEqual(expected_message, response_message)
        self.assertEqual(expected_key, response_key)

        # Ensure that the existing Benchmark was not removed from the database
        self.assertEqual(1, models.Benchmark.objects.filter(
            benchmark_id=db_benchmark.benchmark_id).count())
        # Ensure that all benchmarks are still in the database
        self.assertEqual(initial_benchmarks, models.Benchmark.objects.count())

    @httpmock.activate
    def test_delete_non_existing_benchmark(self):
        """Test deletion of a benchmark that does not exist on MyData."""
        from mydata_benchmarks import models
        # Create 3 benchmarks and place the first of the list in db_benchmark
        initial_benchmarks = 3
        benchmarks = self.submit_benchmarks_default(count=initial_benchmarks)

        last_benchmark = benchmarks[initial_benchmarks - 1]

        # Ensure that a benchmark id does not exist
        non_existing_id = last_benchmark.id + 1
        with self.assertRaises(models.Benchmark.DoesNotExist):
            models.Benchmark.objects.get(pk=non_existing_id)

        # Use bulk delete API to delete the benchmark.
        self.client.force_login(self.user)
        resp = self.client.post(reverse('delete_bulk'), data={'samples[]': str(non_existing_id)})

        # Ensure we return 200 even if a benchmark was not found
        self.assertEqual(200, resp.status_code)
        resp_json = resp.json()
        # Check that no message is present
        self.assertIsNone(resp_json['message'])
        # Check that no sample deletion was reported in the response
        self.assertEqual(0, len(resp_json['results']['samplesDeleted']))
        # Check that one sample is reported as non deleted
        self.assertEqual(1, len(resp_json['results']['samplesNotDeleted']))

        sample_not_deleted = resp_json['results']['samplesNotDeleted'][0]
        # Check that the correct benchmark is being reported
        self.assertEqual(non_existing_id, sample_not_deleted['key'])
        # Check that the proper info is displayed for the non-deleted benchmarks
        self.assertEqual('Not found', sample_not_deleted['message'])

        # Ensure that all benchmarks are still in the database
        self.assertEqual(initial_benchmarks, models.Benchmark.objects.count())

    @httpmock.activate
    def test_delete_benchmark_while_connection_error(self):
        """Try deleting a benchmark and raise Connection error to OpenData."""
        from mydata_benchmarks import models, opendata
        from requests.exceptions import ConnectionError

        # Create one benchmark
        db_benchmark: models.Benchmark = self.submit_benchmarks_default()[0]

        # Mock the OpenData response for deleting the benchmark
        httpmock.add(
            'DELETE', opendata.url(f'api/benchmarks/{db_benchmark.manage_id}'),
            body=ConnectionError()
        )

        # Use bulk delete API to delete the benchmark.
        self.client.force_login(self.user)
        resp = self.client.post(reverse('delete_bulk'), data={'samples[]': str(db_benchmark.pk)})

        self.assertEqual(200, resp.status_code)
        response_message = resp.json()['results']['samplesNotDeleted'][0]['message']
        self.assertIn(f'Could not delete benchmark {db_benchmark.id}', response_message)

        # Ensure that the existing Benchmark was not removed from the database
        self.assertEqual(1, models.Benchmark.objects.filter(
            benchmark_id=db_benchmark.benchmark_id).count())

    @httpmock.activate
    def test_multiple_benchmarks_deletion_connection_error(self):
        """Try deleting multiple benchmarks, when a connection error is raised."""
        from mydata_benchmarks import models, opendata
        from requests.exceptions import ConnectionError

        initial_benchmarks_count = 4
        benchmarks_to_delete_count = 2
        # Create 4 benchmarks and place the last two of the list in benchmarks_to_delete
        benchmarks_to_delete = self.submit_benchmarks_default(
            count=initial_benchmarks_count)[-benchmarks_to_delete_count:]

        # Create mock responses for benchmark deletions
        for b in benchmarks_to_delete:
            httpmock.add(
                'DELETE', opendata.url(f'api/benchmarks/{b.manage_id}'),
                body=ConnectionError()
            )

        # Build list of ids to be deleted
        ids_to_delete = [str(b.id) for b in benchmarks_to_delete]
        # Use bulk delete API to delete the benchmark.
        self.client.force_login(self.user)
        resp = self.client.post(reverse('delete_bulk'), data={'samples[]': ids_to_delete})

        # Check the response
        self.assertEqual(200, resp.status_code)
        resp_json = resp.json()
        response_message = resp_json['results']['samplesNotDeleted'][0]['message']
        self.assertIn(f'Could not delete benchmark {benchmarks_to_delete[0].id}', response_message)

        # Ensure that samplesNotDeleted contains only the first benchmark we attempted to delete
        self.assertEqual(1, len(resp_json['results']['samplesNotDeleted']))
        self.assertEqual(benchmarks_to_delete[0].id,
                         resp_json['results']['samplesNotDeleted'][0]['key'])

        # Ensure that the existing Benchmarks were not removed from the database
        self.assertEqual(initial_benchmarks_count, models.Benchmark.objects.count())
        httpmock.reset()

    @httpmock.activate
    def test_delete_unsynced_benchmark(self):
        """Try to delete a local benchmark that hasn't been synced to Open Data yet."""
        from mydata_benchmarks import models, opendata

        # Create 3 benchmarks and place the first of the list in db_benchmark
        initial_benchmarks = 3
        db_benchmark: models.Benchmark = self.submit_benchmarks_default(count=initial_benchmarks)[0]

        # Ensure the presence of the Benchmarks in the database
        self.assertEqual(initial_benchmarks, models.Benchmark.objects.count())

        # Erase the benchmark_id and manage_id to mimick not having synced db_benchmark yet.
        db_benchmark.benchmark_id = ''
        db_benchmark.manage_id = ''
        db_benchmark.save()

        # Use bulk delete API to delete the benchmark.
        self.client.force_login(self.user)
        resp = self.client.post(reverse('delete_bulk'), data={'samples[]': str(db_benchmark.pk)})

        self.assertEqual(200, resp.status_code)
        # Check the response content
        self.assertEquals('Sample deleted', resp.json()['message'])

        # Check that only that Benchmark was deleted
        with self.assertRaises(models.Benchmark.DoesNotExist):
            models.Benchmark.objects.get(pk=db_benchmark.id)

        # Check that other benchmarks still exist
        self.assertEqual(initial_benchmarks - 1, models.Benchmark.objects.count())
