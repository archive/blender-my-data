import urllib.parse

from django.conf import settings
from django.test import TestCase
from django.urls import reverse

from ..versions import is_benchmark_client_outdated as bco
from ..versions import latest_benchmark_client

from .abstract_benchmark_test import AbstractBenchmarkTest, httpmock


class BenchmarkClientVersionTest(TestCase):
    def test_sanity(self):
        # Just a sanity check that we're still testing against the expected version
        self.assertEqual('1.0b2', str(latest_benchmark_client()))

    def test_valid_versions(self):
        self.assertTrue(bco('0.1'))
        self.assertTrue(bco('1.0b1'))
        self.assertTrue(bco('1.0b2.dev0'))

        self.assertFalse(bco('1.0b3.dev0'))
        self.assertFalse(bco('1.0b3'))
        self.assertFalse(bco('1.1.dev0'))
        self.assertFalse(bco('1.1'))
        self.assertFalse(bco('2'))
        self.assertFalse(bco('1.0b3+ubuntu-3.2'))

    def test_missing_versions(self):
        self.assertTrue(bco(''))
        self.assertTrue(bco(None))


class BenchmarkSubmitVersionCheckTest(AbstractBenchmarkTest):

    # The current version is already handled by test_benchmark_submission.py
    def test_outdated_version(self):
        self._test_outdated_version('1.0b1')
        self._test_outdated_version('0.3')

    @httpmock.activate
    def _test_outdated_version(self, client_version: str):
        benchmark = self.benchmark_copy()
        benchmark['data']['benchmark_client']['client_version'] = client_version

        self.opendata_mock_happy_submission()
        resp = self.submit_benchmark(benchmark)
        self.assertEqual(201, resp.status_code)

        outdated_endpoint = reverse('client_outdated', args=(client_version,))
        next_url = 'http://opendata.local:8002/benchmark/BENCHMARK-ID'
        expect_url = f'http://testserver{outdated_endpoint}' \
                     f'?next={urllib.parse.quote(next_url)}'

        self.assertEqual(expect_url, resp['Location'])

    @httpmock.activate
    def test_missing_version(self):
        benchmark = self.benchmark_copy()
        del benchmark['data']['benchmark_client']

        self.opendata_mock_happy_submission()
        resp = self.submit_benchmark(benchmark)
        self.assertEqual(201, resp.status_code)

        outdated_endpoint = reverse('client_outdated', args=('unknown',))
        next_url = 'http://opendata.local:8002/benchmark/BENCHMARK-ID'
        expect_url = f'http://testserver{outdated_endpoint}' \
                     f'?next={urllib.parse.quote(next_url)}'

        self.assertEqual(expect_url, resp['Location'])

    @httpmock.activate
    def test_no_minimum_version(self):
        # Empty string should skip obsoleteness checks.
        self.set_latest_version('')

        benchmark = self.benchmark_copy()
        benchmark['data']['benchmark_client']['client_version'] = '0.3'

        self.opendata_mock_happy_submission()
        resp = self.submit_benchmark(benchmark)
        self.assertEqual(201, resp.status_code)

        self.assertEqual('http://opendata.local:8002/benchmark/BENCHMARK-ID', resp['Location'])
