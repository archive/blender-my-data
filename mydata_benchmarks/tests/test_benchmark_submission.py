import json
import requests
import typing

from django.urls import reverse

from .abstract_benchmark_test import AbstractBenchmarkTest, httpmock
from .. import models


class BenchmarkSubmissionTest(AbstractBenchmarkTest):

    @httpmock.activate
    def test_happy_flow(self):
        from mydata_benchmarks import models

        benchmark_id = 'BENCHMARK-ID'
        manage_id = 'MANAGE-ID'

        self.opendata_mock_happy_submission()
        benchmark = self.benchmark_copy()
        resp = self.submit_benchmark(benchmark)
        self.assertEqual(201, resp.status_code)

        r_json = resp.json()
        self.assertEqual(benchmark_id, r_json['benchmark_id'])

        # The manage ID should only remain in the MyData database, and not
        # be sent to anyone.
        self.assertNotIn('manage_id', r_json)

        # Test that we did store the benchmark data correctly.
        db_bmark: models.Benchmark = models.Benchmark.objects.get(
            benchmark_id=r_json['benchmark_id'])

        self.assertEqual(manage_id, db_bmark.manage_id)
        self.assertEqual(benchmark_id, db_bmark.benchmark_id)
        self.assertEqual(benchmark, db_bmark.data_raw)
        self.assertEqual(self.user, db_bmark.user)

        # Check the Location header
        self.assertEqual(f'http://opendata.local:8002/benchmark/{benchmark_id}', resp['Location'])

    @httpmock.activate
    def test_bad_opendata_token(self):
        from mydata_benchmarks import opendata

        token = self.generate_token()
        benchmark = self.benchmark_copy()

        # Mock that our OpenData auth token is invalid.
        httpmock.add(
            'POST', opendata.url('api/benchmarks/'),
            json={'no': 'really-not'},
            status=401,
        )
        resp = self.client.post(reverse('submit_benchmark'),
                                HTTP_AUTHORIZATION=f'Bearer {token}',
                                content_type='application/json',
                                data=json.dumps(benchmark))
        self.assertEqual(503, resp.status_code)

    @httpmock.activate
    def test_opendata_unreachable(self):
        self.assertEqual(0, models.Benchmark.objects.count())

        benchmark = self.benchmark_copy()
        resp = self.submit_benchmark(benchmark)
        self.assertEqual(201, resp.status_code)

        queued_url = reverse('submission_queued', kwargs={'pk': 0})
        redirect_url = resp['Location']
        self.assertIn(queued_url[:-1], redirect_url)

        # The Benchmark Client expects the 'benchmark_id' key to be there.
        payload = resp.json()
        self.assertEqual('', payload['benchmark_id'])

        self.assertEqual(1, models.Benchmark.objects.count())
        db_bmark = models.Benchmark.objects.first()
        self.assertEqual('', db_bmark.benchmark_id)
        self.assertEqual('', db_bmark.manage_id)

        # We should be able to GET the returned URL.
        resp = self.client.get(redirect_url)
        self.assertEqual(200, resp.status_code)

        # After syncing, that same URL should redirect to OpenData.
        db_bmark.benchmark_id = 'one-2-three-4'
        db_bmark.save()
        resp = self.client.get(redirect_url)
        self.assertEqual(302, resp.status_code)
        self.assertEqual('http://opendata.local:8002/benchmark/one-2-three-4', resp['Location'])

    @httpmock.activate
    def test_opendata_unreachable_two_queued(self):
        self.assertEqual(0, models.Benchmark.objects.count())

        benchmark = self.benchmark_copy()
        resp = self.submit_benchmark(benchmark)
        self.assertEqual(201, resp.status_code)

        resp = self.submit_benchmark(benchmark)
        self.assertEqual(201, resp.status_code)

        self.assertEqual(2, models.Benchmark.objects.count())

    @httpmock.activate
    def test_opendata_unreachable_and_old_client(self):
        self.assertEqual(0, models.Benchmark.objects.count())

        benchmark = self.benchmark_copy()
        benchmark['data']['benchmark_client']['client_version'] = '0.1'
        resp = self.submit_benchmark(benchmark)
        self.assertEqual(201, resp.status_code)

        queued_url = reverse('submission_queued', kwargs={'pk': 0})
        without_pk0 = queued_url[:-1]

        outdated_url = reverse('client_outdated', kwargs={'current_version': '0.1'})

        self.assertIn(without_pk0, resp['Location'])
        self.assertIn(outdated_url, resp['Location'])

        # The Benchmark Client expects the 'benchmark_id' key to be there.
        payload = resp.json()
        self.assertEqual('', payload['benchmark_id'])

        self.assertEqual(1, models.Benchmark.objects.count())
        db_bmark = models.Benchmark.objects.first()
        self.assertEqual('', db_bmark.benchmark_id)
        self.assertEqual('', db_bmark.manage_id)

    @httpmock.activate
    def test_forwarded_for(self):
        from mydata_benchmarks import opendata

        benchmark_id = 'BENCHMARK-ID'
        manage_id = 'MANAGE-ID'

        def check_request(request: requests.PreparedRequest):
            self.assertEqual('jemoeder:47', request.headers['X-Forwarded-For'])

            resp_body = {'manage_id': manage_id,
                         'benchmark_id': benchmark_id}
            headers: typing.Dict[str, str] = {}
            return 201, headers, json.dumps(resp_body)

        httpmock.add_callback(
            'POST', opendata.url('api/benchmarks/'),
            callback=check_request,
            content_type='application/json',
        )

        benchmark = self.benchmark_copy()
        resp = self.submit_benchmark(benchmark, REMOTE_ADDR='jemoeder:47')
        self.assertEqual(201, resp.status_code)
