import copy
import json
from pathlib import Path

from django.conf import settings
from django.contrib.auth import get_user_model
from django.test import TestCase, override_settings
from django.urls import reverse
import responses

from mydata_benchmarks.opendata import LATEST_SCHEMA_VERSION

UserModel = get_user_model()
httpmock = responses.RequestsMock()


@override_settings(BLENDER_OPENDATA={
    'BASE_URL': 'http://opendata.local:8002/',
    'TOKEN': 'magic-token'})
class AbstractBenchmarkTest(TestCase):
    benchmark_data: dict

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.user = UserModel.objects.create_user('test@user.com', '123456')
        # Load once; use self.benchmark_copy() to get a mutable copy.
        cls.benchmark_data = cls.load_benchmark()

    def setUp(self):
        super().setUp()

        # Make sure we always test with predictable settings.
        self.set_latest_version('1.0b2')

    @staticmethod
    def load_benchmark(schema_version: int=LATEST_SCHEMA_VERSION) -> dict:
        """Load benchmark bytes from examples/benchmark.json.

        :param schema_version: loads 'benchmark-v{schema_version}.json'.
        """
        import mydata

        top_dir = Path(mydata.__file__).absolute().parent.parent
        fpath = top_dir / 'examples' / f'benchmark-v{schema_version}.json'
        with fpath.open('rb') as infile:
            return json.load(infile)

    @staticmethod
    def set_latest_version(version: str):
        """Update Django setting LATEST_CLIENT_VERSIONS['BENCHMARK'] and clear LRU cache."""
        from mydata_benchmarks import versions
        settings.LATEST_CLIENT_VERSIONS['BENCHMARK'] = version
        versions.latest_benchmark_client.cache_clear()

    def benchmark_copy(self):
        # Copy to prevent interaction between tests
        return copy.deepcopy(self.benchmark_data)

    def generate_token(self) -> str:
        self.client.force_login(self.user)
        resp = self.client.post(reverse('generate_token'))
        assert 201 == resp.status_code
        return resp.json()['token']

    def opendata_mock_happy_submission(self, manage_id='MANAGE-ID', benchmark_id='BENCHMARK-ID'):
        from mydata_benchmarks import opendata

        # TODO(Sybren): the mocked response is how we should be able to accept the
        # OpenData response; it's not exactly what is returned by OpenData though.
        httpmock.add(
            'POST', opendata.url('api/benchmarks/'),
            json={'manage_id': manage_id,
                  'benchmark_id': benchmark_id},
            status=201,
        )

    def opendata_mock_happy_deletion(self, manage_id):
        from mydata_benchmarks import opendata

        # Callback to ensure that the OpenData token is used
        def request_callback(request):
            open_data_token = settings.BLENDER_OPENDATA['TOKEN']
            self.assertIn('Authorization', request.headers)
            self.assertEqual(f'Bearer {open_data_token}', request.headers['Authorization'])
            return 204, {}, ''

        # Mock the OpenData response for deleting the benchmark
        httpmock.add_callback(
            'DELETE', opendata.url(f"api/benchmarks/{manage_id}"),
            callback=request_callback,
        )

    def submit_benchmark(self, benchmark: dict, **extra):
        token = self.generate_token()
        resp = self.client.post(reverse('submit_benchmark'),
                                HTTP_AUTHORIZATION=f'Bearer {token}',
                                content_type='application/json',
                                data=json.dumps(benchmark),
                                **extra)
        return resp

    def submit_benchmarks_default(self, count: int = 1) -> list:
        """Given a count, create an equal amount of benchmarks.

        Returns a list of model.Benchmark objects.
        """
        from mydata_benchmarks import opendata, models

        assert (count >= 1), 'Count must be at least one'

        benchmarks = []
        for i in range(count):
            # Mock the OpenData response
            self.opendata_mock_happy_submission(manage_id=f'MG-ID-{i}', benchmark_id=f'BM-ID-{i}')
            resp = self.submit_benchmark(self.benchmark_copy())
            # Fetch the Benchmark from the database
            db_benchmark = models.Benchmark.objects.get(benchmark_id=resp.json()['benchmark_id'])
            benchmarks.append(db_benchmark)
            # Remove the mocked response
            httpmock.remove('POST', opendata.url('api/benchmarks/'))
        return benchmarks


