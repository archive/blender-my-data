from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

from mydata_benchmarks.models import ClientToken

UserModel = get_user_model()


class TokenGeneratorTest(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.user = UserModel.objects.create_user('test@user.com', '123456')
        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        try:
            cls.user.delete()
        except AttributeError:
            pass

    def test_happy_flow(self):
        self.assertEqual(0, ClientToken.objects.count())

        self.client.force_login(self.user)
        resp = self.client.get(reverse('generate_token'),
                               {'auth_callback': 'http://localhost:5555'})
        assert 200 == resp.status_code
        self.assertEqual(0, ClientToken.objects.count())

        resp = self.client.post(reverse('generate_token'))
        assert 201 == resp.status_code

        self.assertEqual(1, ClientToken.objects.count())
        db_token = ClientToken.objects.first()
        assert db_token.token == resp.json()['token']

    def test_token_validation(self):
        url = reverse('verify_token')
        resp = self.client.get(url)
        assert 403 == resp.status_code

        resp = self.client.get(url, HTTP_AUTHORIZATION='Bearer nonexistingtoken')
        assert 403 == resp.status_code

        self.client.force_login(self.user)
        resp = self.client.post(reverse('generate_token'))
        assert 201 == resp.status_code

        token = resp.json()['token']
        resp = self.client.get(url, HTTP_AUTHORIZATION=f'Bearer {token}')
        assert 204 == resp.status_code


class ClientTokenRevokeTest(TestCase):
    def setUp(self):
        self.user = UserModel.objects.create_user('harry@user.com', '123456')
        self.token = self.user.client_tokens.create(token='the_token', hostname='ws-harry')

    def test_revoke_happy(self):
        self.client.force_login(self.user)

        resp = self.client.post(reverse('revoke_token'), {'token_id': self.token.id})
        self.assertEquals(200, resp.status_code)
        # Verify that the ClientToken was actually deleted
        with self.assertRaises(ClientToken.DoesNotExist):
            ClientToken.objects.get(pk=self.token.id)

    def test_revoke_unauthenticated(self):
        resp = self.client.post(reverse('revoke_token'), {'token_id': self.token.id})
        self.assertEquals(302, resp.status_code)

    def test_revoke_token_not_submitted(self):
        self.client.force_login(self.user)

        resp = self.client.post(reverse('revoke_token'))
        self.assertEquals(400, resp.status_code)
        self.assertEquals('No token provided', resp.json()['message'])

    def test_revoke_not_valid(self):
        self.client.force_login(self.user)

        # Sending an empty token id is not allowed
        resp = self.client.post(reverse('revoke_token'), {'token_id': None})
        self.assertEquals(400, resp.status_code)
        self.assertEquals('The token id is not valid', resp.json()['message'])

        # Sending an string that can't be cast to int is not allowed
        resp = self.client.post(reverse('revoke_token'), {'token_id': 'a string'})
        self.assertEquals(400, resp.status_code)
        self.assertEquals('The token id is not valid', resp.json()['message'])

    def test_revoke_not_own_token(self):
        another_user = UserModel.objects.create_user('ronald@user.com', '123456')
        another_token = another_user.client_tokens.create(token='the_other_token', hostname='ws-ronald')

        self.client.force_login(self.user)

        resp = self.client.post(reverse('revoke_token'), {'token_id': another_token.id})
        self.assertEquals(400, resp.status_code)
        self.assertEquals('This token id is not valid', resp.json()['message'])
