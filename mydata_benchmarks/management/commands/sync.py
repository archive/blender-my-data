import logging
import time

import django.db.models
from django.conf import settings
from django.core.management.base import BaseCommand
import requests.exceptions

from ... import models, opendata

log = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Sync any unsynced benchmarks to Open Data'
    session = opendata.session()

    def add_arguments(self, parser):
        parser.add_argument('--flush', '-f',
                            action='store_true',
                            default=False,
                            dest='do_flush',
                            help='Really perform the flush; without this only the '
                                 'size of the queue is shown.')
        parser.add_argument('--monitor', '-m',
                            action='store_true',
                            default=False,
                            dest='do_monitor',
                            help='Continually monitors the queue; implies --flush.')

    def handle(self, *args, do_flush: bool, verbosity: int, do_monitor: bool, **options):
        levels = {
            0: logging.ERROR,
            1: logging.WARNING,
            2: logging.INFO,
            3: logging.DEBUG,
        }
        level = levels.get(verbosity, logging.DEBUG)
        logging.getLogger('mydata_benchmarks').setLevel(level)
        logging.disable(level - 1)
        if do_monitor:
            return self.monitor(verbosity)

        to_sync_query = self.find_to_sync()
        count = to_sync_query.count()

        verbose = verbosity > 0
        if verbose:
            if count == 0:
                self.stdout.write(self.style.SUCCESS(f'The queue is empty'))
            elif count == 1:
                self.stdout.write(self.style.WARNING(f'There is 1 queued item'))
            else:
                self.stdout.write(self.style.WARNING(f'There are {count} queued items'))

        if not do_flush:
            if count > 0:
                self.stdout.write('Use the --flush CLI option to perform flush.')
            return

        if verbose:
            self.stdout.write('Flushing...')

        queue_size = self.find_to_sync().count()
        for idx, benchmark in enumerate(self.find_to_sync()):
            if verbosity > 1:
                self.stdout.write(f'Syncing benchmark {idx+1}/{queue_size}')

            keep_going = self.sync_benchmark(benchmark, verbosity)
            if not keep_going:
                self.stdout.write(self.style.WARNING(f'Flush aborted...'))
                return

        if verbose:
            self.stdout.write('Flush performed...')

            new_count = self.find_to_sync().count()
            if new_count == 0:
                self.stdout.write(self.style.SUCCESS('The queue is now empty'))
            elif new_count == 1:
                self.stdout.write(self.style.WARNING(f'There is still 1 item queued.'))
            else:
                self.stdout.write(self.style.WARNING(f'There are still {new_count} items queued.'))

    def find_to_sync(self) -> django.db.models.QuerySet:
        return models.Benchmark.objects.filter(benchmark_id='')

    def monitor(self, verbosity: int):
        """Keeps monitoring the queue."""

        try:
            while True:
                self._monitor_iteration(verbosity)
                time.sleep(1)
        except KeyboardInterrupt:
            log.info('shutting down webhook queue monitor')

    def _monitor_iteration(self, verbosity: int):
        """Single iteration of queue monitor."""

        queue_size = self.find_to_sync().count()
        if queue_size == 0:
            log.debug('nothing queued')
            return
        self.handle(do_flush=True, do_monitor=False, verbosity=verbosity)

    def sync_benchmark(self, benchmark: models.Benchmark, verbosity: int) -> bool:
        url = opendata.benchmarks_api_url()
        headers = {"Authorization": "Token " + settings.BLENDER_OPENDATA['TOKEN']}

        if verbosity > 2:
            self.stdout.write(f'Sending benchmark to {url}')
        try:
            r = self.session.post(url, json=benchmark.data_raw, verify=True, headers=headers)
        except requests.exceptions.ConnectionError as ex:
            self.stdout.write(self.style.ERROR(f'Unable to reach {url}: {ex}'))
            return False

        if r.status_code in {401, 403}:
            self.stdout.write(self.style.ERROR(
                f'Our token was rejected by OpenData, configure BLENDER_OPENDATA["token"]: '
                f'{r.content}'))
            return False

        if r.status_code != 201:
            # Probably something wrong with this particular submission.
            response = r.text[:120]
            self.stdout.write(self.style.WARNING(
                f'Response code {r.status_code} received from {url}: {response}'))
            return True

        payload: dict = r.json()
        benchmark.benchmark_id = payload['benchmark_id']
        benchmark.manage_id = payload['manage_id']
        benchmark.save()
        return True
