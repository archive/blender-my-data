import json

from django.contrib import admin
from . import models


@admin.register(models.Benchmark)
class BenchmarkAdmin(admin.ModelAdmin):
    model = models.Benchmark

    list_display = ('id', 'user', 'benchmark_id', 'date_created', 'is_synced')
    list_display_links = list_display
    list_filter = ('date_created', )
    raw_id_fields = ['user']

    fields = ['user', 'benchmark_id', 'date_created', 'data']
    readonly_fields = ['date_created', 'benchmark_id', 'data']

    def is_synced(self, benchmark: models.Benchmark) -> str:
        if benchmark.benchmark_id:
            return ''
        return 'NO'

    def data(self, benchmark: models.Benchmark) -> str:
        from django.utils.html import format_html
        return format_html('<pre>{}</pre>', json.dumps(benchmark.data_raw, indent=4))


@admin.register(models.ClientToken)
class ClientTokenAdmin(admin.ModelAdmin):
    model = models.ClientToken

    list_display = ('user', 'token', 'hostname', 'date_created')
    list_filter = ('date_created',)
    search_fields = ('hostname', 'token')
