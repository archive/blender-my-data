from urllib.parse import urljoin

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from django.db import models


class Benchmark(models.Model):
    """The result of a benchmark run on the Benchmark Client."""
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=False, related_name='benchmarks')

    # A blank benchmark_id indicates that it wasn't synced to Open Data yet.
    benchmark_id = models.CharField(max_length=48, editable=False, db_index=True, blank=True)
    manage_id = models.CharField(max_length=48, editable=False, blank=True)
    date_created = models.DateTimeField(auto_now_add=True, db_index=True)
    data_raw = JSONField(editable=False)  # The original result of a benchmark

    class Meta:
        ordering = ('-date_created',)

    def get_absolute_url(self) -> str:
        """Return absolute URL to the benchmark on OpenData."""
        return urljoin(settings.BLENDER_OPENDATA['BASE_URL'], f'benchmark/{self.benchmark_id}')


class ClientToken(models.Model):
    """Used by Benchmark Client to submit data as a User."""
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='client_tokens')
    token = models.CharField(max_length=200, db_index=True)
    hostname = models.CharField(max_length=100, blank=True)  # The hostname of the Benchmark client
    date_created = models.DateTimeField(auto_now_add=True)
    # TODO(fsiddi) add a date_last_used to show the activity of a client in the UI

    class Meta:
        ordering = ('-date_created',)
        verbose_name = 'Client Token'
