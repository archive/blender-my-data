from django.urls import path

from mydata_benchmarks.views import benchmark_api, client_auth, pages

# TODO(Sybren): convert all pattern names to 'benchmark' namespace.
# app_name = 'benchmarks'
urlpatterns = [
    path('', pages.index, name='index'),

    path('token/generate', client_auth.GenerateClientTokenView.as_view(), name='generate_token'),
    path('token/verify', client_auth.verify_token_view, name='verify_token'),
    path('token/revoke', client_auth.revoke_token_view, name='revoke_token'),

    path('benchmarks/submit', benchmark_api.benchmark_submit_view, name='submit_benchmark'),
    path('benchmarks/delete/', benchmark_api.benchmark_delete_bulk_view, name='delete_bulk'),
    path('benchmarks/outdated/<current_version>', benchmark_api.outdated_view,
         name='client_outdated'),
    path('benchmarks/queued/<int:pk>', benchmark_api.submission_queued,
         name='submission_queued'),
]
