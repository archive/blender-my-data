import logging
import uuid
import urllib.parse

from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponseBadRequest, HttpResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.http import require_http_methods
from django.views.generic import View

from mydata_benchmarks.models import ClientToken
from mydata_benchmarks.decorators import client_token_required

log = logging.getLogger(__name__)


class GenerateClientTokenView(View):
    """Create a Client auth token for the logged-in user."""

    log = logging.getLogger(f'{__name__}.GenerateClientTokenView')

    @method_decorator(login_required)
    def post(self, request):
        self.log.info('Generating client token for user %s', request.user)
        hostname = request.POST.get('hostname', '')

        client_token = ClientToken.objects.create(
            user=request.user,
            token=uuid.uuid4().hex,  # TODO sign the token, and maybe embed user id into it?
            hostname=hostname,
        )

        return JsonResponse({'message': 'All was well.', 'token': client_token.token},
                            status=201)

    @method_decorator(login_required)
    def get(self, request):
        """Show a page that performs a POST request to our own URL."""

        # TODO(Sybren): use self.log instead of log
        auth_callback = request.GET.get('auth_callback')
        if not auth_callback:
            log.debug('request to %s rejected, missing auth_callback param', request.path)
            return HttpResponseBadRequest('missing auth_callback')

        parsed_url = urllib.parse.urlparse(auth_callback)
        valid_url = (parsed_url.scheme in {'http', 'https'} and
                     not parsed_url.password and
                     not parsed_url.username and
                     not parsed_url.fragment and
                     not parsed_url.query and
                     parsed_url.hostname == 'localhost')
        if not valid_url:
            log.debug('request to %s rejected, invalid auth_callback URL %s',
                      request.path, auth_callback)
            return HttpResponseBadRequest('malformed auth_callback')

        return render(request, 'token_generate.html', {
            'post_to': request.path,
            'token_hostname': request.GET.get('hostname', ''),
            'success_url': auth_callback,
        })


@client_token_required
def verify_token_view(request):
    # If a client gets here, the token was valid.
    return HttpResponse(status=204)


@require_http_methods(["POST"])
@login_required
def revoke_token_view(request):
    """Revoke a ClientToken by deleting it from the database."""

    token_id = request.POST.get('token_id')

    if not token_id:
        return JsonResponse({'message': 'No token provided'}, status=400)

    try:
        token_id = int(token_id)
    except ValueError:
        log.debug('Invalid token id: %s' % token_id)
        return JsonResponse({'message': 'The token id is not valid'}, status=400)

    try:
        token = request.user.client_tokens.get(pk=token_id)
    except ClientToken.DoesNotExist:
        log.debug('Token %i not found for user %i' % (token_id, request.user.id))
        return JsonResponse({'message': 'This token id is not valid'}, status=400)

    # Delete the token
    token.delete()
    log.debug('Revoked token with id %i' % token_id)

    return JsonResponse({'message': 'Token revoked'}, status=200)
