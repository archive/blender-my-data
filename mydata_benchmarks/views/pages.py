import urllib.parse

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.shortcuts import render


@login_required
def index(request):
    current_user = request.user
    samples_list = current_user.benchmarks.all().order_by('-date_created')
    paginator = Paginator(samples_list, 25)
    page = request.GET.get('page')
    samples = paginator.get_page(page)
    client_tokens = current_user.client_tokens.all()[:5]

    return render(request, 'index.html', {
        'user': request.user,
        'samples': samples,
        'client_tokens': client_tokens,
        'show_url': urllib.parse.urljoin(settings.BLENDER_OPENDATA['BASE_URL'], 'api/benchmarks'),
    })
