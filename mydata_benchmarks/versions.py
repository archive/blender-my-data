"""Checks for version information in the samples.

Requires the latest versions of clients to be configured in the
settings['LATEST_CLIENT_VERSIONS'] dict.
"""
import functools
import logging
import typing

from django.conf import settings
import packaging.version

log = logging.getLogger(__name__)


@functools.lru_cache(maxsize=1)
def latest_benchmark_client() -> packaging.version.Version:
    """Parse Benchmark Client version from Django settings"""

    latest: str = settings.LATEST_CLIENT_VERSIONS['BENCHMARK']
    parsed = packaging.version.parse(latest)
    if isinstance(parsed, packaging.version.LegacyVersion):
        log.error("Malformed LATEST_CLIENT_VERSIONS['BENCHMARK'] setting: %r", latest)
    else:
        log.info('Configured latest version of the Benchmark Client is %s', parsed)
    return parsed


def is_benchmark_client_outdated(client_version: typing.Optional[str]) -> bool:
    if not client_version:
        return True
    parsed_ver = packaging.version.parse(client_version)
    return parsed_ver < latest_benchmark_client()
