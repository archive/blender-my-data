import functools
import urllib.parse

from django.conf import settings
import requests


LATEST_SCHEMA_VERSION = 2


def url(url_suffix: str) -> str:
    """Construct a URL pointing to OpenData.

    >>> url('some/path/')
    'http://opendata.local/some/path/'
    """

    return urllib.parse.urljoin(settings.BLENDER_OPENDATA['BASE_URL'], url_suffix)


@functools.lru_cache(maxsize=1)
def benchmarks_api_url() -> str:
    """Return a URL to the benchmarks API endpoint."""
    return url('api/benchmarks/')


@functools.lru_cache(maxsize=1)
def session() -> requests.Session:
    """Create a Requests Session for communication with OpenData."""
    from requests.adapters import HTTPAdapter

    sess = requests.Session()
    sess.mount('https://', HTTPAdapter(max_retries=5))

    return sess
