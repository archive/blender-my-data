from django.apps import AppConfig


class MydataBenchmarksConfig(AppConfig):
    name = 'mydata_benchmarks'
    verbose_name = 'Benchmarks'
