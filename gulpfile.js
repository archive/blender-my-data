let argv         = require('minimist')(process.argv.slice(2));
let autoprefixer = require('gulp-autoprefixer');
let cache        = require('gulp-cached');
let concat       = require('gulp-concat');
let gulp         = require('gulp');
let gulpif       = require('gulp-if');
let pug          = require('gulp-pug');
let livereload   = require('gulp-livereload');
let plumber      = require('gulp-plumber');
let sass         = require('gulp-sass');
let sourcemaps   = require('gulp-sourcemaps');
let uglify       = require('gulp-uglify');

let enabled = {
	failCheck: !argv.production,
	maps: argv.production,
	prettyPug: !argv.production,
	uglify: argv.production
};

/* Root of Blender Web Assets */
let pathBWA = 'websrc/blender-web-assets';

/* Stylesheets */
gulp.task('styles', function(done) {
	gulp
		.src('websrc/styles/**/*.sass')
		.pipe(gulpif(enabled.failCheck, plumber()))
		.pipe(gulpif(enabled.maps, sourcemaps.init()))
		.pipe(sass({outputStyle: 'compressed'}))
		.pipe(autoprefixer("last 3 versions"))
		.pipe(gulpif(enabled.maps, sourcemaps.write(".")))
		.pipe(gulp.dest('webstatic/css'))
		.pipe(gulpif(argv.livereload, livereload()));
	done();
});


/* Templates - Pug */
gulp.task('templates', function(done) {

	/* Order matters. First compile BWA, then the local project.
	 * If local templates have the same name as those in BWA, they will be used.
	 * e.g. `websrc/templates/_footer.pug` will override the one from BWA. */
	let pugs = [
		'websrc/blender-web-assets/templates/**/*.pug',
		'websrc/templates/**/*.pug'
	];

	gulp.src(pugs)
		.pipe(gulpif(enabled.failCheck, plumber()))
		.pipe(cache('templating'))
		.pipe(pug({
				pretty: enabled.prettyPug
		}))
		.pipe(gulp.dest('templates/'))
		.pipe(gulpif(argv.livereload, livereload()));
	done();
});


/* Collection of scripts in websrc/scripts/tutti/ to merge into tutti.min.js */
/* Since it's always loaded, it's only for functions that we want site-wide */
gulp.task('scripts_tutti', function(done) {
	gulp.src('websrc/scripts/tutti/**/*.js')
		.pipe(gulpif(enabled.failCheck, plumber()))
		.pipe(gulpif(enabled.maps, sourcemaps.init()))
		.pipe(concat("tutti.min.js"))
		.pipe(gulpif(enabled.uglify, uglify()))
		.pipe(gulpif(enabled.maps, sourcemaps.write(".")))
		.pipe(gulp.dest('webstatic/js/generated/'))
		.pipe(gulpif(argv.livereload, livereload()));
	done();

	gulp.src(pathBWA + '/scripts/tutti/*.js')
		.pipe(gulpif(enabled.failCheck, plumber()))
		.pipe(gulpif(enabled.maps, sourcemaps.init()))
		.pipe(concat("tutti_bwa.min.js"))
		.pipe(gulpif(enabled.uglify, uglify()))
		.pipe(gulpif(enabled.maps, sourcemaps.write(".")))
		.pipe(gulp.dest('webstatic/js/generated/'))
		.pipe(gulpif(argv.livereload, livereload()));
	done();
});


/* Simply move these vendor scripts from node_modules  */
gulp.task('scripts_vendor', function(done) {

	let toMove = [
		'node_modules/jquery/dist/jquery.min.js',
		'node_modules/toastr/build/toastr.min.js',
		'node_modules/js-cookie/src/js.cookie.js'
	];

	gulp.src(toMove)
		.pipe(gulp.dest('webstatic/js/vendor/'));
	done();
});


// Move fonts to the static folder.
gulp.task('assets_fonts', function(done) {
	gulp.src(pathBWA + '/fonts/**/*')
		.pipe(gulp.dest('webstatic/fonts/'));
	done();
});

/* Copy favicon.ico from blender-web-assets to webstatic */
gulp.task('move_favicon', function(done) {
	gulp.src(pathBWA + '/images/favicon.ico')
		.pipe(gulp.dest('webstatic/images/'));
	done();
});

gulp.task('move_all_assets', gulp.series('scripts_vendor', 'assets_fonts', 'move_favicon'));


// While developing, run 'gulp watch'
gulp.task('watch', function(done) {
	// Only reload the pages if we run with --livereload
	if (argv.livereload){
		livereload.listen();
	}

	// Watch for changes in files for this project and Blender Web Assets

	gulp.watch('websrc/styles/**/*.sass', gulp.series('styles'));
	gulp.watch(pathBWA + '/styles/**/*.sass', gulp.series('styles'));

	gulp.watch('websrc/templates/**/*.pug', gulp.series('templates'));
	gulp.watch(pathBWA + '/templates/**/*.pug', gulp.series('templates'));

	gulp.watch('websrc/scripts/tutti/*.js', gulp.series('scripts_tutti'));
	gulp.watch(pathBWA + '/scripts/tutti/*.js', gulp.series('scripts_tutti'));
});

// Run 'gulp' to build everything at once
gulp.task('default', gulp.series('styles', 'templates', 'scripts_tutti', 'move_all_assets'));
