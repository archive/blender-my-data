from django.contrib import admin

# Configure the admin site. Easier than creating our own AdminSite subclass.
# Text to put at the end of each page's <title>.
admin.site.site_title = 'Blender My Data'
# Text to put in each page's <h1>.
admin.site.site_header = 'Blender My Data'
