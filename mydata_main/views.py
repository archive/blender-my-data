import logging

from django.views.generic import TemplateView
from django.http import JsonResponse


log = logging.getLogger(__name__)


class ErrorView(TemplateView):
    """Renders an error page."""
    status = 500

    json_messages = {
        400: 'Bad Request. Your HTTP client sent a request that we did not understand.',
        403: 'Forbidden. For some reason you are not allowed to do this.',
        404: 'Not Found. The page you tried to reach does not exist.',
        500: 'Server Error. Oops, we made a mistake somewhere.',
    }

    def dispatch(self, request, *args, **kwargs):
        from django.http.response import HttpResponse
        if request.method in {'HEAD', 'OPTIONS'}:
            # Don't render templates in this case.
            return HttpResponse(status=self.status)

        # Return JSON if the request has a XMLHttpRequest header
        if request.is_ajax():
            return JsonResponse({'message': self.json_messages[self.status]}, status=self.status)

        # We allow any method for this view,
        response = self.render_to_response(self.get_context_data(**kwargs))
        response.status_code = self.status
        return response


def csrf_failure(request, reason=""):
    import django.views.csrf

    return django.views.csrf.csrf_failure(request,
                                          reason=reason,
                                          template_name='errors/403_csrf.html')

