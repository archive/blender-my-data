from django.apps import AppConfig


class MydataMainConfig(AppConfig):
    name = 'mydata_main'
    verbose_name = 'My Data'
