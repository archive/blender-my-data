#!/bin/bash

# Run as `submit_benchmark.sh benchmark.json your-token`
# You can get your-token by looking up or creating a mydata_benchmarks_clienttoken
# record associated with your user in the database.

JSON_FNAME="$1"; shift
AUTH_TOKEN="$1"; shift

curl_opts=(
    -H "Content-Type: application/json"
    -H "Accept: application/json"
    -H "Authorization: Bearer $AUTH_TOKEN"
    --data-binary "@${JSON_FNAME}"
)
curl "${curl_opts[@]}" "$@" http://mydata.local:8003/benchmarks/submit
