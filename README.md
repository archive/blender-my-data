# Blender My Data

Portal to manage own benchmark results.

## Requirements

- The example config expects a running version of [Blender My Data](https://mydata.blender.org/) under
http://mydata.local:8003/ for development. We don't have a production version yet.

- You also need a APP-ID and APP-SECRET from [Blender ID](https://blender.org/id)
or your local version of Blender ID (http://id.local:8000)

## Development setup

After cloning the Git repo, perform these steps to create a working dev server:

* Make sure PostgreSQL is running and it has a `mydata` or similar database available
* `git submodule init` and `git submodule update`
* Copy `mydata/settings.example.py` to `mydata/settings.py` and adjust to your needs
* Start your virtaulenv with `pipenv shell`
* Install requirements with `pipenv install`
* Run migrations  `./manage.py migrate`
* Install frontend dependencies `./gulp`
* Run application `./manage.py runserver 8003`
* Configure Blender ID to have an OAuth Client with redirect URL
   http://mydata.local:8003/oauth/authorized
* Configure a cronjob to run `pipenv run ./manage.py sync --flush -v0`


## Try it out

1. Submit an example benchmark with a valid Blender ID access_token
`$ sh examples/submit_benchmark.sh examples/benchmark-v2.json your-blender-id-token`
You have to create the access_token manually at the moment since we
don't have an authorized benchmark tool yet

2. Open running application in Browser, login with Blender ID and manage your benchmarks.


## Migrating from post-OAuth plugin era

In November 2018 we moved from using the `oauth` module in this project (which is now deleted,
so don't bother searching for it except in the Git history) to using the
[Blender ID OAuth Client](https://gitlab.com/blender-institute/blender-id-oauth-client)
Django app. Since this was kinda hard to get correctly working with Django migrations, follow these
steps instead:

    pipenv run ./manage.py migrate
    pipenv run ./manage.py migrate blender_id_oauth_client 0001
    pipenv run ./manage.py dbshell
    drop table blender_id_oauth_client_oauthtoken;
    alter table oauth_oauthtoken rename to blender_id_oauth_client_oauthtoken;
    alter sequence oauth_oauthtoken_id_seq rename to blender_id_oauth_client_oauthtoken_seq;
    \q (or Ctrl+D)
    pipenv run ./manage.py migrate

This rolls back the `blender_id_oauth_client` migrations to a point in time where it used the same
database model as mydata, then moves the mydata token table to the new Django app. After that, the
migrations are re-run to split the tokens table into tokens + OAuth user mapping.
