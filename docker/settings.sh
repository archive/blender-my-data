DOCKER_TAG="armadillica/blender-mydata"
DEPLOY_BRANCH=${DEPLOY_BRANCH:-production}
GIT_URL_MYDATA="git://git.blender.org/blender-my-data.git"
GIT_URL_OPENDATA="git://git.blender.org/blender-open-data.git"
