#!/bin/bash -e

# macOS does not support readlink -f, so we use greadlink instead
if [[ `uname` == 'Darwin' ]]; then
    command -v greadlink 2>/dev/null 2>&1 || { echo >&2 "Install greadlink using brew."; exit 1; }
    readlink='greadlink'
else
    readlink='readlink'
fi

MY_DIR="$(dirname "$($readlink -f "$0")")"
cd "$MY_DIR"

source ./settings.sh

ROOT="$(dirname "$MY_DIR")"
STAGINGDIR="$ROOT/docker/staging"
PROJECT_NAME="mydata"

rm -rf "$STAGINGDIR"/{mydata,opendata}
mkdir -p $STAGINGDIR
REMOTE_ROOT="$STAGINGDIR/$PROJECT_NAME"

# Check that production branch has been pushed.
if [ -n "$(git log origin/$DEPLOY_BRANCH..$DEPLOY_BRANCH --oneline)" ]; then
    echo "WARNING: not all changes to the $DEPLOY_BRANCH branch have been pushed."
    echo "Press [ENTER] to continue deploying current origin/$DEPLOY_BRANCH, CTRL+C to abort."
    read dummy
fi

function git_clone() {
    REPO_NAME="$1"
    URL="$2"
    BRANCH="$DEPLOY_BRANCH"

    set -e
    echo "==================================================================="
    echo "CLONING REPO ON $REPO_NAME @$BRANCH"
    git -C "$STAGINGDIR" clone --depth 1 --branch $BRANCH $URL $REPO_NAME
    git -C "$STAGINGDIR/$REPO_NAME" submodule init
    git -C "$STAGINGDIR/$REPO_NAME" submodule update --recommend-shallow

    # We need *some* settings to be able to run `manage.py collectstatic` later.
    # That command is given while building the docker image.
    cp "$MY_DIR/deploytime-settings-$REPO_NAME.py" $STAGINGDIR/$REPO_NAME/$REPO_NAME/settings.py
}

git_clone mydata $GIT_URL_MYDATA
git_clone opendata $GIT_URL_OPENDATA

# Gulp everywhere

# TODO: reinstate this, because it's much faster:
# GULP=$ROOT/node_modules/.bin/gulp
# if [ ! -e $GULP -o gulpfile.js -nt $GULP ]; then
#     npm install
#     touch $GULP  # installer doesn't always touch this after a build, so we do.
# fi
# $GULP --cwd $STAGINGDIR/mydata --production
# $GULP --cwd $STAGINGDIR/opendata --production

pushd "$STAGINGDIR/mydata"
./gulp
rm -rf node_modules
popd
pushd "$STAGINGDIR/opendata"
./gulp
rm -rf node_modules
popd

echo
echo "==================================================================="
echo "Deploy of ${PROJECT_NAME} is ready for dockerisation."
echo "==================================================================="
