FROM armadillica/blender-mydata-base

# Set up My Data
COPY --chown=mydata:django staging/mydata /var/www/mydata/
WORKDIR /var/www/mydata
COPY deploytime-settings-mydata.py /var/www/mydata/mydata/settings.py
RUN pipenv run python3 manage.py collectstatic --noinput

# Set up Open Data
COPY --chown=opendata:django staging/opendata /var/www/opendata/
WORKDIR /var/www/opendata
COPY deploytime-settings-opendata.py /var/www/opendata/opendata/settings.py
RUN pipenv run python3 manage.py collectstatic --noinput
WORKDIR /

# Copy some simple files so that we don't have to rebuild the base image to change them.
COPY root_files/* /
COPY bash_history /root/.bash_history
COPY nginx/snippets/* /etc/nginx/snippets/
COPY nginx/sites-available/* /etc/nginx/sites-available/
COPY nginx/error/* /var/www/error/
COPY uwsgi/* /etc/uwsgi/apps-available/
COPY --chown=elastic:elastic elasticsearch.yml /opt/elasticsearch/config/
