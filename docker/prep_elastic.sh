#!/bin/bash -e

ELASTIC_VERSION=6.3.2
ELASTIC_DIR="elasticsearch-${ELASTIC_VERSION}"
ELASTIC_FILE="elasticsearch-${ELASTIC_VERSION}.tar.gz"

if [ ! -e "$ELASTIC_FILE" ]; then
    echo "Downloading $ELASTIC_FILE"
    URL="https://artifacts.elastic.co/downloads/elasticsearch/$ELASTIC_FILE"
    curl -O "$URL"
fi

[ -d staging ] || mkdir staging
cd staging

rm -rf elasticsearch*
tar zxvf ../$ELASTIC_FILE

# Remove the X-Pack module, as we don't use it anyway.
rm -rfv $ELASTIC_DIR/modules/x-pack/ \
        $ELASTIC_DIR/bin/x-pack*

# The UseConcMarkSweepGC option was deprecated in Java 9, but it's still
# used by ElasticSearch, causing a warning. This deletes it.

# macOS requires a space after sed -i, while Linux does not
if [[ `uname` == 'Darwin' ]]; then
    sed_arg='-i ""'
else
    sed_arg='-i""'
fi

sed $sed_arg '/UseConcMarkSweepGC/d' $ELASTIC_DIR/config/jvm.options
