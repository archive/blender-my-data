"""
Deploy-time settings.

These settings are used during the preparation of the Docker image,
so that we can run `manage.py collectstatic`. The settings should
mimick the production settings, but of course not contain any secrets.

The actual settings should be mounted using a Docker volume.
"""

# noinspection PyUnresolvedReferences
from opendata.common_settings import *

DEBUG = False
SECRET_KEY = r'''1234'''

import sys
import os

if os.path.exists('/var/www/settings/opendata_settings.py'):
    sys.path.append('/var/www/settings')
    from opendata_settings import *
