# MyData dockerisation

To create a Docker image for MyData + OpenData, do the following:

- Make sure that the `production` branch of both MyData and OpenData is up to date.
  Whatever is in that branch *on the Git server* is what will be packaged in the Docker image.
- Run `./prep_elastic.sh` to download ElasticSearch, strip it down, and configure it.
- Run `./prep_docker_img.sh` to prepare mydata & opendata for dockerisation.
- Run `./build_docker_img.sh` to build & push the Docker image.

To build everything at once, use:

    ./prep_elastic.sh && ./prep_docker_img.sh && docker build . -t armadillica/blender-mydata

To update the server, run `./2server.sh mydata.blender.org`

Add this cron job to `/etc/cron.d/mydata` on the host:

    PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
    MAILTO=your-address@example.com

    30   * * * * root docker exec --user opendata mydata /manage-opendata.sh clearsessions
    40   * * * * root docker exec --user mydata   mydata /manage-mydata.sh clearsessions
    */5  * * * * root docker exec --user mydata   mydata /manage-mydata.sh sync --flush -v0


## TLS Certificates

TLS certificates for HTTPS traffic are managed via Let's Encrypt and Træfik.
