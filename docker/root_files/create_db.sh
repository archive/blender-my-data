#!/bin/sh -ex

# This should only be run once, to create the database + users.
mkdir -p $PGDATA
chown -R postgres:postgres $PGDATA
su postgres -c '/usr/lib/postgresql/10/bin/initdb'

echo "Starting postgresql"
pg_ctlcluster 10 main start


echo
echo "Creating 'mydata' user"
su postgres -c '(echo mydata; echo mydata) | createuser mydata -dPERS'

echo
echo "Creating 'opendata' user"
su postgres -c '(echo opendata; echo opendata) | createuser opendata -dPERS'

su postgres -c 'createdb -E UTF8 -O mydata mydata'
su postgres -c 'createdb -E UTF8 -O opendata opendata'

echo
echo '-------------------------------------'
echo 'Database users were created with trivial passwords'
echo 'Please change their passwords using'
echo "ALTER USER \"mydata\" WITH PASSWORD 'new_password';"
echo "ALTER USER \"opendata\" WITH PASSWORD 'new_password';"
echo '-------------------------------------'
