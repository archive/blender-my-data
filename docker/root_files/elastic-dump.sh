#!/bin/bash -e

cd /var/www/opendata
pipenv run ./manage.py elastic_dump "${@}"
