#!/bin/bash

set -e
echo -n "Starting up at "
date -Iseconds

# Start nginx first, so that it can at least serve a "we'll be right back" page.
echo "Starting nginx"
nginx

# Do this before we start Postgres so we shut that down
# even when something else doesn't want to start.
function shutdown {
    echo "Shutting down"
    set +e

    [ -e /var/run/uwsgi-mydata.pid   ] && uwsgi --stop /etc/uwsgi/apps-enabled/mydata.ini
    [ -e /var/run/uwsgi-opendata.pid ] && uwsgi --stop /etc/uwsgi/apps-enabled/opendata.ini

    [ -e $ELASTIC_PID ] && kill $(<$ELASTIC_PID)
    pg_ctlcluster 10 main stop
    nginx -s stop
}
trap shutdown EXIT

# Configure and start Postgres.
if [ ! -e $PGDATA ]; then
    echo "$PGDATA does not exist, initialising database"
    . /create_db.sh
else
    echo "Starting postgresql"
    pg_ctlcluster 10 main start
fi


mkdir -p /var/log/{apps,uwsgi,nginx,postgresql}/
chgrp django /var/log/apps
chmod g+ws /var/log/apps
[ -e /var/log/apps/opendata.log ] && chown opendata:django /var/log/apps/opendata.log
[ -e /var/log/apps/mydata.log ] && chown mydata:django /var/log/apps/mydata.log

echo "Starting ElasticSearch"
chown -R elastic:elastic /opt/elasticsearch/{data,logs}
su elastic -c "/opt/elasticsearch/bin/elasticsearch --daemonize --pidfile $ELASTIC_PID"

echo "Running migrations"
cd /var/www/mydata
pipenv run python3 manage.py migrate
cd /var/www/opendata
pipenv run python3 manage.py migrate

echo "Starting Redis"
mkdir -m 755 -p /var/run/redis/  # directory for unix-domain socket
mkdir -m 750 -p /var/log/redis/
chown redis:redis /var/run/redis/ /var/log/redis/
su redis -c 'cd /var/lib/redis/; /usr/bin/redis-server /etc/redis/redis-mydata.conf'
(sleep 2; echo -n 'Clearing Redis cache: '; echo FLUSHALL | redis-cli -s /var/run/redis/redis.sock) &

echo "Starting uWSGI"
uwsgi /etc/uwsgi/apps-enabled/mydata.ini
uwsgi /etc/uwsgi/apps-enabled/opendata.ini

echo "Waiting for stuff"
set +e
tail -f /dev/null &
KILLPID=$!

function finish {
    echo "Finishing Docker image"
    set -x
    kill $KILLPID
}
trap finish QUIT
trap finish TERM
trap finish INT

wait

echo "Done waiting, shutting down some stuff cleanly"
